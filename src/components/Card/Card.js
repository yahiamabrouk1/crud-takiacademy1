import React from "react";
import articles from "../../constant/article";
import "./card.css";
function Card(props) {
  return (
    <div>
      {articles.map((subj) => (
        <div key={subj.id}>
          <h2 className="card-title">{subj.title}</h2>
          <p> {subj.description} </p>
        </div>
      ))}
    </div>
  );
}
export default Card;
